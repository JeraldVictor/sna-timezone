(() => {
  //   Get Current Date
  let newDate = new Date();

  // get and set the current Time
  let timePlaceHolder = document.getElementById("timeHolder");
  timePlaceHolder.innerHTML = `Time : ${newDate.toLocaleTimeString("en-IN")}`;

  //   get and set client's Time Zone
  let timezonePlaceHolder = document.getElementById("timezoneHolder");
  timezonePlaceHolder.innerHTML = `TimeZone : ${
    Intl.DateTimeFormat().resolvedOptions().timeZone
  }`;

  //   get and set date
  let datePlaceHolder = document.getElementById("dateHolder");
  datePlaceHolder.innerHTML = `Date : ${newDate.toLocaleDateString("en-IN")}`;
})();
